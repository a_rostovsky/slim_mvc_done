<?php
namespace App\Controller;

use App\Services\GameService;
use Slim\Views\Twig as View;

class GameController
{
    /**
    * @var gameService
    */
    private $gameService;

    public function __construct(View $view, gameService $gameService) {
        $this->view = $view;
        $this->gameService = $gameService;
    }

    public function index($request, $response)
    {
        return $response->getBody()->write("Hello, Slim2");
    }

    public function getGame($request, $response, $args)
    {
        $game = $this->gameService->getGameInfo($args['id']);
        $format = $request->getQueryParams()['format'];
        if ($format == 'json' || $format === null) {
            return $response->withJson([
                'game' => $game
            ], 200);
        }

        if ($format == 'html') {
            return $this->view->render($response, 'index.html', [
                'game' => $game
            ]);
        }
    }

    public function getList($request, $response, $args)
    {
        $games = $this->gameService->getAllGames();
        $format = $request->getQueryParams()['format'];
        if ($format == 'json' || $format === null) {
            return $response->withJson([
                'games' => $games
            ], 200);
        }

        if ($format == 'html') {
            return $this->view->render($response, 'list.html', [
                'games' => $games
            ]);
        }
    }

}