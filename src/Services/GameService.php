<?php
namespace App\Services;

class GameService
{

    /**
     * @var \PDO
     */
    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function getGameInfo($id)
    {
        $query = $this->db->prepare('SELECT * FROM mvc.game where id = :id');
        $query->bindParam(':id', $id, \PDO::PARAM_INT);
        $query->execute();
        $game = $query->fetchAll(\PDO::FETCH_ASSOC);

        if(isset($game[0])) {
            return $game[0];
        } else {
            throw new \Exception('not found game with id: '. $id, 422);
        }
    }

    public function getAllGames()
    {
        $query = $this->db->prepare('SELECT * FROM mvc.game');
        $query->execute();
        $listOfAllGames = $query->fetchAll(\PDO::FETCH_ASSOC);
        return $listOfAllGames;
    }
}