<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Controller\GameController;
use App\Services\GameService;

require __DIR__ . '/../vendor/autoload.php';

$config = [
    'settings' => [
        'db' => [
            'engine' => 'mysql',
            'host' => 'localhost',
            'dbname' => 'mvc',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
            'collation' => 'utf8_general_ci'
        ],
    ],
];

$app = new \Slim\App($config);

// Get container
$container = $app->getContainer();


// Register component on container
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(__DIR__ . '/../views/', []);
    $basePath = rtrim(str_ireplace('index.php', '', $container->get('request')->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container->get('router'), $basePath));

    return $view;
};

$container['GameService'] = function($c) {
    $db = $c->get('db');
    return new GameService($db);
};

$container['GameController'] = function($c) {
    $view = $c->get("view"); 
    $GameService = $c->get("GameService");
    return new GameController($view, $GameService);
};

$container['db'] = function ($c) {
    $db = $c->get('settings')['db'];
    $pdo = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'], $db['username'], $db['password']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};


// $app->get('/{name}', function (Request $request, Response $response, array $args) {
// 	$name = $args['name'];
// 	return $response->getBody()->write("There are you, $name");
// 	return $response->withJson([
// 	    'name' => $name
// 	], 201);

//     // return $response;
// });
require __DIR__ . '/../src/router.php';
$app->run();